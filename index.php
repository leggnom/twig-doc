<?
	error_reporting(E_ALL);
	ini_set("display_errors", 1);



	define('root', $_SERVER['DOCUMENT_ROOT']);
	define('template', root . '/template/');

	$DATA = array();

	require_once root . '/Twig/Autoloader.php';
	Twig_Autoloader::register();

	$loader = new Twig_Loader_Filesystem(template);
	$twig = new Twig_Environment($loader, array(
		'cache' => root.'cache',
		));
	$twig->addExtension(new Twig_Extension_Debug());

	require_once root . '/user_function.php';


	require_once root . '/controller/base.php';