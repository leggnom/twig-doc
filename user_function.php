<?
$twig = new Twig_Environment($loader);
$twig->addFunction(new Twig_SimpleFunction('code', function ($text, $return = false) {
	$text = str_replace(array(
		'{', '}', '<', '>'
		), array(
		'&#123;', '&#125;', '&lt;', '&gt;'
		), $text);

	$text = '<pre><code class="code-style">' . $text . '</code></pre>';
	
	if($return){
		return $text;
	}else{
		echo $text;
	}
}));