<?
    $url = rawurldecode($_SERVER['REQUEST_URI']);
    if($url[0] == '/') $url = substr($url, 1);
    if(substr($url, -1) == '/') $url = substr ($url, 0, -1);
    

    $template = 'index.twig';
    if($url){
    	$template = 'page/' . $url . '/' . $template;
    }

    $DATA['page'] = array();
    $DATA['page']['url'] = $url;
    
    $DATA['menu'] = array(
    	array(
    		'url' => 'documentation',
    		'name' => 'Документация')
    	);

    foreach($DATA['menu'] as $k => $v){
    	if($v['url'] == $url){
    		$DATA['menu'][$k]['active'] = true;
        }
    }


    $DATA = loadController($url, $DATA);



    if(!file_exists(template . $template)){
        header("HTTP/1.0 404 Not Found");
        header("Status: 404 Not Found");
        $template = '/404.twig';
    }
    if(isset($_GET['show']) && $_GET['show'] === 'data'){
        echo '<pre>';
        var_dump($DATA);
    }else{
        echo $twig->render($template, $DATA);
    }



    function loadController($url, $DATA){
        $url = explode('/', $url);
        $path = '';
        foreach($url as $v){
            $path .= $v . '/';
            $file = root . 'controller/' . $path . 'index.php';
            if(file_exists($file)){
                require_once $file;
            }
        }
        return $DATA;
    }